<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= wp_head(); ?>
</head>

<body>
<main class="inner-page">

    <header>
        <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-header">
                    <?php
                    $logo = sqhr_theme_option("header-logo");
                    if ($logo && strlen($logo['url'])):?>

                        <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>">
                            <img src="<?= $logo['url'] ?>" alt="<?php bloginfo('name'); ?>"/>
                        </a>

                    <?php endif; ?>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                            aria-expanded="false" aria-controls="navbar">
                        <span></span>
                    </button>
                </div>
                <!-- ends navbar-header -->
                <div id="navbar" class="navbar-collapse collapse">
                    <?php wp_nav_menu(array(
                        'menu_class' => 'nav navbar-nav navbar-right',
                        'container'  => 'ul',
                        'menu-item'  => ''
                    )); ?>
                </div>
            </div>
        </nav>
        <!-- ends navbar -->
    </header>
    <div class="actions-wrap">
        <div class="container">
            <a class="back" href="javascript:history.go(-1)">Back</a>
        </div>
    </div>