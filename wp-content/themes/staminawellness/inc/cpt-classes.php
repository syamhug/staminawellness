<?php

add_action('init', 'post_type_classes');


$classes                        = array();
$classes['posttypeid']          = "classes";
$classes['posttypeid_singular'] = "classes";
$classes['title_singular']      = "Classes";
$classes['title_plural']        = "Classes";

function post_type_classes()
{
    global $classes;

    // Set labels
    $labels = array(
        'name'               => _x($classes['title_plural'], 'post type general name'),
        'singular_name'      => _x($classes['posttypeid_singular'], 'post type singular name'),
        'add_new'            => _x('Add New', $classes['title_singular']),
        'add_new_item'       => __('Add New ' . $classes['title_singular']),
        'edit_item'          => __('Edit ' . $classes['title_singular']),
        'new_item'           => __('New ' . $classes['title_singular']),
        'all_items'          => __('All ' . $classes['title_plural']),
        'view_item'          => __('View ' . $classes['title_singular']),
        'search_items'       => __('Search ' . $classes['title_plural']),
        'not_found'          => __('No ' . $classes['title_plural'] . ' found'),
        'not_found_in_trash' => __('No ' . $classes['title_plural'] . ' found in the Trash'),
        'parent_item_colon'  => '',
        'people_name'        => $classes['title_plural']
    );

    register_post_type(
        'classes', array(
            'labels'          => $labels,
            'public'          => true,
            'menu_icon'       => 'dashicons-welcome-learn-more',
            'show_ui'         => true,
            '_builtin'        => false, // It's a custom post type, not built in
            '_edit_link'      => 'post.php?post=%d',
            'capability_type' => 'page',
            'hierarchical'    => true,
            'rewrite'         => array("slug" => "classes"), // Permalinks
            'query_var'       => "classes",
            'taxonomies'    => array('brand'),
            'menu_position'   => 23,
            'supports'        => array('title','editor','thumbnail','excerpt')
        )
    );

    flush_rewrite_rules(false);
}
