<?php

# Reset Dashboard widgets
function custom_remove_dashboard_widgets()
{
    global $wp_meta_boxes;
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); # Allows for basic post entry
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']); # Shows you who is linking to you
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); # Highlights entries from the WordPress team on WordPress.org
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); # Displays stats about your blog
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); # Displays the most recent comments on your blog
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); # Displays your most recent drafts
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); # Displays the WordPress Planet feed, which includes blog entries from WordPress.org
    #unset( $wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins'] ); # Displays new, updated, and popular WordPress plugins on WordPress.org
}

add_action('wp_dashboard_setup', 'custom_remove_dashboard_widgets');

# Reset Admin menu items
function remove_menus()
{
    remove_menu_page('jetpack');                    //Jetpack*
    remove_menu_page('edit.php');                   //Posts
    remove_menu_page('edit-comments.php');          //Comments
    #remove_menu_page( 'index.php' );                  //Dashboard
    #remove_menu_page( 'edit.php?post_type=acf' );     // ACF
    #remove_menu_page( 'index.php' );                  //Dashboard
    #remove_menu_page( 'upload.php' );                 //Media
    #remove_menu_page( 'edit.php?post_type=page' );    //Pages
    #remove_menu_page( 'themes.php' );                 //Appearance
    #remove_menu_page( 'plugins.php' );                //Plugins
    #remove_menu_page( 'users.php' );                  //Users
    #remove_menu_page( 'tools.php' );                  //Tools
    #remove_menu_page( 'options-general.php' );        //Settings

}

add_action('admin_menu', 'remove_menus');

# Remove wordpress Logo and comments
function no_wp_logo_admin_bar_remove()
{
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');
    $wp_admin_bar->remove_menu('comments');
}

add_action('wp_before_admin_bar_render', 'no_wp_logo_admin_bar_remove', 0);

# Add Custom Logo
add_action('admin_head', 'admin_css');
function admin_css()
{
    echo '<link rel="stylesheet" type="text/css" href="' . get_template_directory_uri()
        . '/assets/stylesheets/admin.css">';
}

#Admin login Page Logo change

function replace_wp_logo()
{
    echo '<style type="text/css">h1 a {
    background-image:url(' . get_bloginfo('template_directory') . '/assets/images/admin/logo.png) !important;
    width:100%!important; background-size:100%!important;
    height: 106px!important;  }</style>';
}

add_action('login_head', 'replace_wp_logo');

# Admin Change Login url
function change_wp_login_url()
{
    return home_url();
}

add_filter('login_headerurl', 'change_wp_login_url');

#Remove EMOJI script and style
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

#Remove default menu active class
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
    if( in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}

#Remove Image thumbnail width height
function remove_img_attr ($html) {
    return preg_replace('/(width|height)="\d+"\s/', "", $html);
}

add_filter( 'post_thumbnail_html', 'remove_img_attr' );