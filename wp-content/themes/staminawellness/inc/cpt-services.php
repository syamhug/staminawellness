<?php

add_action('init', 'post_type_services');


$services                        = array();
$services['posttypeid']          = "services";
$services['posttypeid_singular'] = "services";
$services['title_singular']      = "Services";
$services['title_plural']        = "Services";

function post_type_services()
{
    global $services;

    // Set labels
    $labels = array(
        'name'               => _x($services['title_plural'], 'post type general name'),
        'singular_name'      => _x($services['posttypeid_singular'], 'post type singular name'),
        'add_new'            => _x('Add New', $services['title_singular']),
        'add_new_item'       => __('Add New ' . $services['title_singular']),
        'edit_item'          => __('Edit ' . $services['title_singular']),
        'new_item'           => __('New ' . $services['title_singular']),
        'all_items'          => __('All ' . $services['title_plural']),
        'view_item'          => __('View ' . $services['title_singular']),
        'search_items'       => __('Search ' . $services['title_plural']),
        'not_found'          => __('No ' . $services['title_plural'] . ' found'),
        'not_found_in_trash' => __('No ' . $services['title_plural'] . ' found in the Trash'),
        'parent_item_colon'  => '',
        'people_name'        => $services['title_plural']
    );

    register_post_type(
        'services', array(
            'labels'          => $labels,
            'public'          => true,
            'menu_icon'       => 'dashicons-admin-generic',
            'show_ui'         => true,
            '_builtin'        => false, // It's a custom post type, not built in
            '_edit_link'      => 'post.php?post=%d',
            'capability_type' => 'page',
            'hierarchical'    => true,
            'rewrite'         => array("slug" => "services"), // Permalinks
            'query_var'       => "services",
            'taxonomies'    => array('brand'),
            'menu_position'   => 23,
            'supports'        => array('title','editor','thumbnail','excerpt')
        )
    );

    flush_rewrite_rules(false);
}
