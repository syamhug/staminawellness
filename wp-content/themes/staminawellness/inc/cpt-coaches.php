<?php

add_action('init', 'post_type_coaches');


$coaches                        = array();
$coaches['posttypeid']          = "coaches";
$coaches['posttypeid_singular'] = "coaches";
$coaches['title_singular']      = "Coaches";
$coaches['title_plural']        = "Coaches";

function post_type_coaches()
{
    global $coaches;

    // Set labels
    $labels = array(
        'name'               => _x($coaches['title_plural'], 'post type general name'),
        'singular_name'      => _x($coaches['posttypeid_singular'], 'post type singular name'),
        'add_new'            => _x('Add New', $coaches['title_singular']),
        'add_new_item'       => __('Add New ' . $coaches['title_singular']),
        'edit_item'          => __('Edit ' . $coaches['title_singular']),
        'new_item'           => __('New ' . $coaches['title_singular']),
        'all_items'          => __('All ' . $coaches['title_plural']),
        'view_item'          => __('View ' . $coaches['title_singular']),
        'search_items'       => __('Search ' . $coaches['title_plural']),
        'not_found'          => __('No ' . $coaches['title_plural'] . ' found'),
        'not_found_in_trash' => __('No ' . $coaches['title_plural'] . ' found in the Trash'),
        'parent_item_colon'  => '',
        'people_name'        => $coaches['title_plural']
    );

    register_post_type(
        'coaches', array(
            'labels'          => $labels,
            'public'          => true,
            'menu_icon'       => 'dashicons-groups',
            'show_ui'         => true,
            '_builtin'        => false, // It's a custom post type, not built in
            '_edit_link'      => 'post.php?post=%d',
            'capability_type' => 'page',
            'hierarchical'    => true,
            'rewrite'         => array("slug" => "coaches"), // Permalinks
            'query_var'       => "coaches",
            'taxonomies'    => array('brand'),
            'menu_position'   => 23,
            'supports'        => array('title','editor','thumbnail')
        )
    );

    flush_rewrite_rules(false);
}
