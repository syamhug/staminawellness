<?php

add_action('init', 'post_type_news');


$news                        = array();
$news['posttypeid']          = "news";
$news['posttypeid_singular'] = "news";
$news['title_singular']      = "News";
$news['title_plural']        = "News";

function post_type_news()
{
    global $news;

    // Set labels
    $labels = array(
        'name'               => _x($news['title_plural'], 'post type general name'),
        'singular_name'      => _x($news['posttypeid_singular'], 'post type singular name'),
        'add_new'            => _x('Add New', $news['title_singular']),
        'add_new_item'       => __('Add New ' . $news['title_singular']),
        'edit_item'          => __('Edit ' . $news['title_singular']),
        'new_item'           => __('New ' . $news['title_singular']),
        'all_items'          => __('All ' . $news['title_plural']),
        'view_item'          => __('View ' . $news['title_singular']),
        'search_items'       => __('Search ' . $news['title_plural']),
        'not_found'          => __('No ' . $news['title_plural'] . ' found'),
        'not_found_in_trash' => __('No ' . $news['title_plural'] . ' found in the Trash'),
        'parent_item_colon'  => '',
        'people_name'        => $news['title_plural']
    );

    register_post_type(
        'news', array(
            'labels'          => $labels,
            'public'          => true,
            'menu_icon'       => 'dashicons-megaphone',
            'show_ui'         => true,
            '_builtin'        => false, // It's a custom post type, not built in
            '_edit_link'      => 'post.php?post=%d',
            'capability_type' => 'page',
            'hierarchical'    => true,
            'rewrite'         => array("slug" => "news"), // Permalinks
            'query_var'       => "news",
            'taxonomies'    => array('brand'),
            'menu_position'   => 23,
            'supports'        => array('title','editor','thumbnail')
        )
    );

    flush_rewrite_rules(false);
}
