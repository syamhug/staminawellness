<?php

/**
 * Enqueue scripts and styles.
 */
function stamina_scripts() {
    wp_enqueue_style('bootstrap', assets_url() . 'stylesheets/main.css', array(), time(), 'all');
    wp_enqueue_style('style', assets_url() . 'stylesheets/responsive.css', array(), time(), 'all');
    wp_enqueue_style('custom', assets_url() . 'stylesheets/custom.css', array(), time(), 'all');

    wp_deregister_script('jquery');
    wp_deregister_script('wp-embed');
    wp_enqueue_script('jquery', assets_url() . 'js/jquery-2.2.3.min.js', array(), '2.2.3', true);
    wp_enqueue_script('bootstrap', assets_url() . 'js/bootstrap.min.js', array(), '3.3.5', true);
    wp_enqueue_script('owl', assets_url() . 'js/owl.carousel.js', array(), '2.0.0', true);
    wp_enqueue_script('script', assets_url() . 'js/script.min.js', array(), time(), true);
    wp_enqueue_script('custom', assets_url() . 'js/custom.js', array(), time(), true);

}

add_action('wp_enqueue_scripts', 'stamina_scripts');
