<?php
/*
  Template Name: About Page
 */
get_header('about');
?>
<?php if (get_field('banner_image')): ?>
    <section class="banner-sec">
        <div class="banner-inner">
            <img alt="" src="<?= get_field('banner_image'); ?>">
        </div> <!-- ends banner-inner -->
    </section> <!-- ends banner-sec -->
<?php endif; ?>

<section class="about-us">
    <div class="container">
        <div class="title-block"><h2><?=the_title()?></h2></div>
        <div class="main-content">
            <?php
            if ( have_posts() ) : while ( have_posts() ) : the_post();
                echo the_content();
            endwhile; else:
                // no posts found
            endif;
            ?>
        </div> <!-- ends content -->
        <div class="items">
            <div class="row">

                <?php
                $cp_type = 'services';
                if(get_field('services')):
                $cp_args = array(
                    'post_type'      => $cp_type,
                    'post_status'    => 'publish',
                    'posts_per_page' => -1,
                    'post__in'       => get_field('services'),
                    'orderby'        => 'post__in'
                );

                $cp_my_query = new WP_Query($cp_args);
                if ($cp_my_query->have_posts()):

                while ($cp_my_query->have_posts()) : $cp_my_query->the_post();
                ?>
                    <div class="col-sm-4">
                        <div class="item">
                            <div class="img-block">
                                <a href="<?=the_permalink()?>" class="ajax-popup-link"><?= the_post_thumbnail() ?></a>
                            </div>
                            <div class="content">
                                <a href="<?=the_permalink()?>" class="ajax-popup-link"><h2><?= the_title() ?></h2></a>
                                <p><?= the_excerpt() ?></p>
                            </div> <!-- ends content -->
                        </div> <!-- ends item -->
                    </div>

                <?php
                endwhile;
                endif;
                else:
                    print("content not found..");
                endif;
                wp_reset_query();

                ?>

            </div>
        </div> <!-- ends items
    </div> <!-- ends container -->
</section> <!-- ends about-us -->


<?= get_template_part('partials/content', 'flexible'); ?>

<?php
get_footer();
?>
