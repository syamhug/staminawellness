<?php
/*
  Template Name: Contact Page
 */
get_header();
?>
<?php
$location = sqhr_theme_option("location");
$email = sqhr_theme_option("email");
$phone = sqhr_theme_option("phone");
?>
<?php if (get_field('banner')): ?>
    <div class="banner">
        <img class="banner" src="<?= get_field('banner'); ?>" alt="">

        <?php if (get_field('caption')): ?>
            <p><? /*= get_field('banner_caption'); */ ?></p>
        <?php endif; ?>

    </div>
<?php endif; ?>

<div class="container" id="content">

    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-12">
            <h2><?= the_title(); ?></h2>

            <?php if (have_posts()) :
                while (have_posts()) : the_post(); ?>
                    <?php the_content(); ?>
                <?php endwhile; endif; ?>

        </div>
    </div>
    <?= $social_fb = sqhr_theme_option("social-facebook"); ?>

</div>
<?= $location ?>
<?= $email ?>
<?= $phone ?>
<?php
get_footer();
?>
