<?php
/*
  Template Name: GYM Page
 */
get_header('gym');
?>

<?php if (get_field('banner')): ?>
    <section class="banner-sec">
        <div class="banner-inner">
            <img src="<?= get_field('banner') ?>" alt="">
        </div>
        <!-- ends banner-inner -->
    </section> <!-- ends banner-sec -->
<?php endif; ?>

<section class="the-gym-sec">
    <div class="container">
        <div class="title-block"><h2><?= the_title() ?></h2></div>
        <div class="main-content">
            <?php if (have_posts()) :
                while (have_posts()) : the_post(); ?>
                    <?php the_content(); ?>
            <?php endwhile; endif; ?>
        </div>
        <!-- ends content -->

        <div class="items">
            <div class="row">
                <?php

                // check if the repeater field has rows of data
                if (have_rows('items')): // loop through the rows of data
                {
                while (have_rows('items')) : the_row(); ?>

                            <div class="col-sm-4">
                                <div class="item">
                                    <div class="img-block"> <img src="<?=the_sub_field('icon')?>"></div>
                                    <div class="content-block">
                                        <h2><?=the_sub_field('caption')?></h2>

                                        <div class="content">
                                            <ul>
                                                <?php
                                                if (get_sub_field('desc')):
                                                    $items = explode(',', get_sub_field('desc'));
                                                    $items = str_replace('<p>','',$items);
                                                    $items = str_replace('</p>','',$items);
                                                    foreach ($items as $item_list):
                                                        ?>
                                                        <li><?= $item_list; ?></li>
                                                    <?php
                                                    endforeach;
                                                endif;
                                                ?>

                                            </ul>
                                        </div>
                                        <!-- ends content -->
                                    </div>
                                    <!-- ends content-block -->
                                </div>
                                <!-- ends item -->
                            </div>
                <?php

                endwhile;
                } else :

                    // no rows found

                endif;
                ?>
        </div>
        </div>
        <!-- ends items -->

    </div>
    <!-- ends container -->
</section> <!-- ends about-us -->

<?= get_footer() ?>
