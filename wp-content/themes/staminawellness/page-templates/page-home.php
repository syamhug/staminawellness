<?php
/**
 * Template Name: Home page
 * The home template file.
 *
 * @link    https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Staminawellness
 */
get_header();
?>
<?php

// check if the repeater field has rows of data
if (have_rows('slider')):
    ?>
    <section class="banner-sec">
        <div class="banner">

            <?php while (have_rows('slider')) : the_row(); ?>
                <div class="item">
                    <img src="<?= the_sub_field('image'); ?>"/>

                    <div class="content-wrap">
                        <div class="content">
                            <?= the_sub_field('caption'); ?>
                        </div>
                    </div>
                    <!-- ends content -->
                </div>
            <?php endwhile; ?>

        </div>
        <!-- ends banner -->
    </section> <!-- ends banner-sec -->


<?php

else :

    // Home Page Slider not Enabled

endif;
?>


<?= get_template_part('partials/content', 'flexible'); ?>

<div class="sec-blank"></div>
<?php
get_footer();
?>