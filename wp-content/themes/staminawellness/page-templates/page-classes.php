<?php
/*
  Template Name: Classes Page
 */
get_header('classes');
?>

<?php if (get_field('banner')): ?>
    <section class="banner-sec">
        <div class="banner-inner">
            <img src="<?=get_field('banner')?>" alt="">
        </div> <!-- ends banner-inner -->
    </section> <!-- ends banner-sec -->
<?php endif; ?>

<section class="classes-inner">
    <div class="container">
        <div class="row">
            <div class="title-block">
                <h2><?=the_title()?></h2>
            </div>
        </div>
        <div class="items">
            <div id="owl-classes" class="owl-carousel">

                <?php
                $cp_type = 'classes';
                $cp_args = array(
                    'post_type'      => $cp_type,
                    'post_status'    => 'publish',
                    'posts_per_page' => -1,
                    'post__in'       => get_field('class'),
                    'orderby'        => 'post__in'
                );

                $cp_my_query = new WP_Query($cp_args);
                if ($cp_my_query->have_posts()):

                    while ($cp_my_query->have_posts()) :
                        $cp_my_query->the_post();
                ?>

                <div class="item">
                    <div class="img-block">
                        <a href="<?=the_permalink()?>" class="ajax-popup-link"><?=the_post_thumbnail()?></a>
                    </div>
                    <div class="content">
                        <a href="<?=the_permalink()?>" class="ajax-popup-link"><h2><?=the_title()?></h2></a>
                        <p><?=the_excerpt()?></p>
                    </div>
                    <div class="btn-wrap">
                        <a class="btn ajax-popup-link" href="<?=the_permalink()?>">Learn more</a>
                    </div>
                </div>

                <?php
                    endwhile;
                else:
                    print("content not found..");
                endif;
                wp_reset_query();
                ?>

            </div>
        </div>
    </div>
</section>

<?php
get_footer();
?>
