<?php
/*
  Template Name: Careers Page
 */
get_header('career');
?>

<?php if (get_field('banner')): ?>
    <section class="banner-sec">
        <div class="banner-inner">
            <img  src="<?= get_field('banner'); ?>" alt="">
        </div>
        <!-- ends banner-inner -->
    </section> <!-- ends banner-sec -->
<?php endif; ?>

<section class="careers-sec">
    <div class="container">
        <div class="title-block"><h2><?=the_title()?></h2></div>
        <div class="requirements">
            <div class="block">
                <div class="img-block"><img src="<?= get_field('logo'); ?>" alt=""></div>
                <?=get_field('logo_text')?>
            </div> <!-- ends block -->
        </div> <!-- ends requirements -->
    </div> <!-- ends container -->
    <div class="career-requirements">
        <div class="container">
            <div class="row">
                <?php

                // check if the repeater field has rows of data
                if( have_rows('requirements') ):

                // loop through the rows of data
                while ( have_rows('requirements') ) : the_row();
                ?>
                    <div class="col-md-4 col-sm-6">
                        <div class="item">
                            <div class="img-block">
                                <img src="<?= the_sub_field('image'); ?>" >
                            </div>
                            <div class="content">
                                <h2><?=the_sub_field('heading')?></h2>
                                <p><?=the_sub_field('desc')?></p>
                            </div>
                        </div>
                    </div>
                <?php
                endwhile;

                else :

                    // no rows found

                endif;

                ?>

            </div>
        </div>
    </div> <!-- ends career-requirements -->
</section> <!-- ends about-us -->

<?php
get_footer();
?>
