<?php
/*
  Template Name: About Page
 */
get_header('about');
?>
<?php if (get_field('banner_image')): ?>
    <section class="banner-sec">
        <div class="banner-inner">
            <img alt="" src="<?= get_field('banner_image'); ?>">
        </div> <!-- ends banner-inner -->
    </section> <!-- ends banner-sec -->
<?php endif; ?>

<section class="about-us">
    <div class="container">
        <div class="title-block"><h2><?=the_title()?></h2></div>
        <div class="main-content">
            <?php
            if ( have_posts() ) : while ( have_posts() ) : the_post();
                echo the_content();
            endwhile; else:
                // no posts found
            endif;
            ?>
        </div> <!-- ends content -->

    </div> <!-- ends container -->
</section> <!-- ends about-us -->


<?= get_template_part('partials/content', 'flexible'); ?>

<?php
get_footer();
?>
