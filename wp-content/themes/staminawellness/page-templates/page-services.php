<?php
/*
  Template Name: Services Page
 */
get_header('services');
?>

<?php if (get_field('banner_image')): ?>
    <section class="banner-sec">
        <div class="banner-inner">
            <img src="<?=get_field('banner_image')?>" alt="">
        </div> <!-- ends banner-inner -->
    </section> <!-- ends banner-sec -->
<?php endif; ?>

<section class="services">
    <div class="container">
        <div class="title-block"><h2><?=the_title()?></h2>
            <div class="main-content">
                <?php
                if ( have_posts() ) : while ( have_posts() ) : the_post();
                    echo the_content();
                endwhile; else:
                    // no posts found
                endif;
                ?>
            </div> <!-- ends content -->
        </div>

        <div class="items">
            <div class="row">

                <?php
                $cp_type = 'services';
                $cp_args = array(
                    'post_type'      => $cp_type,
                    'post_status'    => 'publish',
                    'posts_per_page' => -1,
                    'post__in'       => get_field('services'),
                    'orderby'        => 'post__in'
                );

                $cp_my_query = new WP_Query($cp_args);
                if ($cp_my_query->have_posts()):

                    while ($cp_my_query->have_posts()) :
                        $cp_my_query->the_post(); ?>

                        <div class="col-sm-4">
                            <div class="item">
                                <div class="img-block">
                                    <a class="ajax-popup-link" href="<?=the_permalink()?>"><?=the_post_thumbnail()?></a>
                                </div>
                                <div class="content">
                                    <a class="ajax-popup-link" href="<?=the_permalink()?>">
                                        <h2><?=the_title()?></h2>
                                    </a>

                                    <p><?=the_excerpt()?></p>
                                </div>
                                <!-- ends content -->
                            </div>
                            <!-- ends item -->
                        </div>


                    <?php endwhile;
                else:
                    print("content not found..");
                endif;
                wp_reset_query();
                ?>

            </div>
        </div> <!-- ends items -->
    </div> <!-- ends container -->
</section> <!-- ends about-us -->


<?php
get_footer();
?>
