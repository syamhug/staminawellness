<?php
/*
  Template Name: Booking Page
 */
get_header('booking');
?>

<?php if (get_field('banner')): ?>
    <section class="banner-sec">
        <div class="banner-inner">
            <img src="<?= get_field('banner'); ?>" alt="">
        </div>
    </section> <!-- ends banner-sec -->
<?php endif; ?>

<section class="booking-sec">
    <div class="container">
        <h2>sign up for your tailored program</h2>

        <div class="row">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                <p>Fill out this form and a membership advisor will contact you to schedule your visit and begin
                    your<br>Stamina experience.</p>

                <div class="form-wrap">
                    <?=do_shortcode('[contactform]')?>
                </div>
                <!-- ends form-wrap -->
            </div>
        </div>
    </div>
    <!-- ends container -->
</section> <!-- ends about-us -->
<?php
get_footer();
?>
