<?php
/*
  Template Name: News & Events Page
 */
get_header('news-and-events');
?>

<?php if (get_field('banner')): ?>
    <section class="banner-sec">
        <div class="banner-inner">
            <img src="<?= get_field('banner'); ?>" alt="">
        </div>
    </section> <!-- ends gym-sec -->
<?php endif; ?>

<section class="classes-inner">
    <div class="container">
        <div class="row">
            <div class="title-block">
                <h2>news and events</h2>
                <p><?= get_field('caption')?>
            </div>
        </div>
        <div class="items">
            <div class="row">
                <div class="item">
                    <div class="img-block">
                        <a href="#"><img src="<?=assets_url()?>/images/news.png"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
