<?php
/*
  Template Name: Contact Page
 */
get_header('contact');
?>
<?php
$location = sqhr_theme_option("location");
$email = sqhr_theme_option("email");
$phone = sqhr_theme_option("phone");
?>
<?php if (get_field('banner')): ?>
    <section class="banner-sec">
        <div class="banner-inner">
            <img src="<?=get_field('banner')?>" alt="">
        </div> <!-- ends banner-inner -->
    </section> <!-- ends banner-sec -->
<?php endif; ?>

<section class="contact-sec">
    <div class="container">
        <div class="title-block">
            <h2><?=the_title()?></h2>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="item">
                    <div class="img-block"><img alt="" src="<?=assets_url()?>/images/ico-map.png"></div>
                    <div class="content">
                        <p><?=$location?></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="item">
                    <div class="img-block"><img alt="" src="<?=assets_url()?>/images/ico-mail.png"></div>
                    <div class="content">
                        <p><a href="mailto:<?=$email?>"><?=$email?></a></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="item">
                    <div class="img-block"><img alt="" src="<?=assets_url()?>/images/ico-call.png"></div>
                    <div class="content">
                        <p><?=$phone?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?=get_footer()?>
