<?php
/**
 * Dashboard Rest
 */
require(get_template_directory() . '/inc/reset-wordpress.php');

/**
 * Theme setup
 *
 * * These are placed after setting up custom post typess
 * * because most of them requires function calls after
 * * CPT setup
 */
require(get_template_directory() . '/inc/theme-setup-helpers.php');
require(get_template_directory() . '/inc/theme-setup.php');
#require(get_template_directory() . '/inc/theme-setup-menus.php');
require(get_template_directory() . '/inc/theme-setup-scripts.php');

/**
 * CPT
 */
require get_template_directory() . '/inc/cpt-classes.php';
require get_template_directory() . '/inc/cpt-coaches.php';
require get_template_directory() . '/inc/cpt-services.php';
require get_template_directory() . '/inc/cpt-news.php';

/**
 * Theme options page
 *
 * Refactored to use Redux
 */
require( get_template_directory() . '/redux/admin-init.php' );