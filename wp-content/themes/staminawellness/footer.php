<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package staminawellness
 */
?>
<?php
$social_youtube = sqhr_theme_option("social-youtube");
$social_instagram = sqhr_theme_option("social-instagram");
$social_twitter = sqhr_theme_option("social-twitter");
$social_fb = sqhr_theme_option("social-facebook");
$copyright = sqhr_theme_option("footer-copyright");
$footer_logo_left = sqhr_theme_option("footer-logo-left");
$footer_logo_right = sqhr_theme_option("footer-logo-right");
?>

<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4">
                <div class="content-wrap">
                    <div class="content">
                        <a href="<?=esc_url(home_url('/'))?>" class="logo-left">
                                <img alt="Stamina Wellness" src="<?= $footer_logo_left['url'] ?>">
                        </a>
                    </div> <!-- ends content -->
                </div> <!-- ends content-wrap -->
            </div>
            <div class="col-sm-4">
                <div class="content-wrap">
                    <div class="content">
                        <ul class="social">
                            <li><a class="fb" href="<?=$social_fb?>" target="_blank"></a></li>
                            <li><a class="tw" href="<?=$social_twitter?>" target="_blank"></a></li>
                            <li><a class="ins" href="<?=$social_instagram?>" target="_blank"></a></li>
                        </ul>
                        <p class="copyright"><?= $copyright ?></p>
                    </div> <!-- ends content -->
                </div> <!-- ends content-wrap -->
            </div>
            <div class="col-sm-4">
                <div class="content-wrap">
                    <div class="content">
                        <a href="#" class="logo-right">
                            <img alt="Swanlake" src="<?= $footer_logo_right['url'] ?>">
                        </a>
                    </div> <!-- ends content -->
                </div> <!-- ends content-wrap -->
            </div>
        </div>
    </div>
</footer>
<?php
wp_footer();
?>
<script>  var ajaxurl = '<?= admin_url('admin-ajax.php'); ?>';</script>
</body>
</html>