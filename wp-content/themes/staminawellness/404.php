<?php
/**
 * 404 Page
 *
 */
?>
<?=get_header()?>

<div class="clearfix"></div>
<div class="content-area">
    <div class="content-area">
        <div id="main" class="container" role="main">

            <section class="error-404 not-found general-section"">
                <header class="page-header">
                    <h1 class="page-title"><?php esc_html_e('Oops! That page can&rsquo;t be found.', 'staminawellness'); ?></h1>
                </header>
                <!-- .page-header -->

                <div class="page-content">
                    <p><?php esc_html_e('It looks like nothing was found at this location.', 'staminawellness'); ?></p>

                </div>
                <!-- .page-content -->
            </section>
            <!-- .error-404 -->

        </div>
        <!-- #main -->
    </div>
    <!-- #primary -->

</div>

<?php
get_footer(); ?>


