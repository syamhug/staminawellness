<?php
get_header();
?>


    <section class="classes-inner">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <p><?= the_title(); ?></p>

                    <p>
                        <?= get_the_post_thumbnail(); ?>
                    </p>

                    <?php
                    while (have_posts()) : the_post();

                        get_template_part('partials/content', get_post_format());


                    endwhile; // End of the loop.
                    ?>

                </div>
                <!-- #main -->

                <div class="col-md-4">
                    <h2>Recent Posts</h2>
                    <ul>
                        <?php
                        $args = array('post_type' => 'news');
                        $recent_posts = wp_get_recent_posts($args);
                        foreach ($recent_posts as $recent) {
                            echo '<li><a href="' . get_permalink($recent["ID"]) . '">' . $recent["post_title"]
                                . '</a> </li> ';
                        }
                        wp_reset_query();
                        ?>
                    </ul>
                </div>
            </div>
            <!-- #primary -->
        </div>
    </section>

<?php
get_footer();
