<?php

/**
 * The template used for displaying flexible content for pages
 */
// check if the flexible content field has rows of data
if (have_rows('flexible_content')) {

    // loop through the rows of data
    while (have_rows('flexible_content')) : the_row();

        /**
         * This will locate and include template part if present in the theme
         * from partials directory
         * 
         * Example :
         * * Layout : divider
         * * Template : partials/flexible/content-divider.php
         */
        $content_layout = get_row_layout();
        if (locate_template('partials/flexible/row-' . $content_layout . '.php') != '') {
            get_template_part('partials/flexible/row', $content_layout);
        }

    endwhile;
}
