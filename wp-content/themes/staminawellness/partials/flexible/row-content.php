<?php
$heading        = get_sub_field('heading');
$content        = get_sub_field('content');
$services       = get_sub_field('services');
$content_blocks = get_sub_field('content_blocks');
$link           = the_sub_field('more');
?>
<div class="brand-manifesto">
    <div class="container-fluid alpha omega img-right">
        <div class="container">
            <h2><?= $heading; ?></h2>

            <div class="col-sm-8 col-sm-offset-2 content">
                <p><?= $content ?></p>
            </div>
            <!-- ends content -->
            <div class="services col-sm-8 col-sm-offset-2">

                <?php

                // check if the repeater field has rows of data
                if (have_rows('content_blocks')): // loop through the rows of data
                {
                    while (have_rows('content_blocks')) : the_row();

                        ?>
                        <div class="col-sm-6">

                            <div class="img-block">
                                <img class="alignnone size-full wp-image-230" src="<?= the_sub_field('image'); ?>"
                                     alt="ico-we-are" width="39" height="41">
                            </div>

                            <h3><?= the_sub_field('heading'); ?></h3>

                            <div class="content">
                                <p><?= the_sub_field('content'); ?></p>
                            </div>
                            <p><!-- ends content --><br>


                                <?php if(get_sub_field("more")): ?>
                                <a class="btn" href="<?php the_sub_field('more'); ?>">Read more</a>
                                <?php endif; ?>

                            </p>

                        </div>

                    <?php
                    endwhile;
                } else :
                    // no rows found
                endif;
                ?>


                <div class="clearfix"></div>
            </div>
            <!-- ends services -->
        </div>
    </div>
</div>