<?php
$image        = get_sub_field('image');
$heading    = get_sub_field('heading');
$content    = get_sub_field('content');
?>
<section class="philosophy-sec" style="background-image: url('<?=$image?>');">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="title"><h2><?=$heading?></h2></div>
                <div class="content">
                    <p><?=$content?></p>
                </div>
            </div>
        </div>
    </div>
</section> <!-- ends philosophy-sec -->
