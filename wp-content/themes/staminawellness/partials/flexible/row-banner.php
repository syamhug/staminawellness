<?php
$type        = get_sub_field('type');
$image       = get_sub_field('image');
$video       = get_sub_field('video');
$youtube_url = get_sub_field('youtube_url');
?>
<div class="container-fluid alpha omega">
    <div class="main-hero">
        <?php if ($type == 'Video'): ?>
            <button class="play" onclick="playPause()">Play/Pause</button>
            <video width="1024" height="410" id="video" loop>
                <source src="<?= $video; ?>" type="video/mp4">
                <source src="<?= str_replace('mp4','ogg',$video); ?>" type="video/mp4">
                Your browser does not support HTML5 video.
            </video>
            <div class="overlay"></div>
            <div class="clearix"></div>
        <?php elseif ($type == 'Youtube Url'): ?>
                <iframe width="1280" height="720" src="https://www.youtube.com/embed/<?= getVideoId($youtube_url); ?>"
                        frameborder="0" allowfullscreen></iframe>
        <?php else: ?>
            <img src="<?= $image; ?>">
        <?php endif; ?>

    </div>
    <!-- ends main-hero -->
</div>