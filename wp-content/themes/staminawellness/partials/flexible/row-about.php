<?php
$title = get_sub_field('title');
$read_more = get_sub_field('read_more');
$banner = get_sub_field("banner");
?>

<section class="about-sec" style="background-image: url('<?= $banner ?>');">
    <div class="container">
        <div class="title">
            <h2><?= $title; ?></h2>
            <a class="btn" href="<?= $read_more; ?>">explore more</a>
        </div> <!-- ends title -->
        <div class="content-block">
            <div class="item-wrap">

                <?php
                $cp_type = 'services';
                $cp_args = array(
                    'post_type'      => $cp_type,
                    'post_status'    => 'publish',
                    'posts_per_page' => -1,
                    'post__in'       => get_sub_field('services'),
                    'orderby'        => 'post__in'
                );

                $cp_my_query = new WP_Query($cp_args);
                if ($cp_my_query->have_posts()):

                while ($cp_my_query->have_posts()) : $cp_my_query->the_post();

                ?>

                    <div class="item">
                        <div class="img-block">
                            <a href="<?php echo the_permalink(); ?>">
                                <img alt="" src="<?= get_field('icon'); ?>">
                            </a>
                        </div>
                        <div class="content">
                            <h2><?= the_title(); ?></h2>
                        </div>
                    </div>

                <?php
                endwhile;
                else:
                    print("content not found..");
                endif;
                wp_reset_query();

                ?>
            </div> <!-- ends item-wrap -->
        </div> <!-- ends content-block -->
    </div>
</section> <!-- ends about-sec -->
