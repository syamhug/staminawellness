<?php
$title        = get_sub_field('title');
$read_more        = get_sub_field('read_more');
$banner = get_sub_field("banner");
?>
<section class="services-sec" style="background-image: url('<?= $banner ?>');">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="box-modal">
                    <div class="content">
                        <h2><?= $title; ?></h2>
                        <div class="buttom-set">
                            <a class="btn" href="<?= $read_more; ?>">explore more</a>
                        </div> <!-- ends buttom-set -->
                    </div> <!-- ends content -->
                </div> <!-- ends box-modal -->
            </div>
        </div>
    </div>
</section> <!-- ends gym-sec -->