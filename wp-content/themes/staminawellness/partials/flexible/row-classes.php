<section class="classes-sec">
    <div class="items">

        <?php
        $cp_type = 'classes';
        $cp_args = array(
            'post_type'      => $cp_type,
            'post_status'    => 'publish',
            'posts_per_page' => -1,
            'post__in'       => get_sub_field('classes'),
            'orderby'        => 'post__in'
        );

        $cp_my_query = new WP_Query($cp_args);
        if ($cp_my_query->have_posts()):

        while ($cp_my_query->have_posts()) : $cp_my_query->the_post();

        ?>

            <div class="item cycling" style="background-image: url('<?= get_field('slider_thumb')?>');">
                <div class="content">
                    <h2><?= the_title(); ?></h2>
                </div>
            </div>

        <?php
        endwhile;
        else:
            print("content not found..");
        endif;
        wp_reset_query();

        ?>

    </div> <!-- ends items -->
    <div class="block-content">
        <h2>Classes</h2>
        <a class="btn" href="<?=get_sub_field('read_more')?>">explore more</a>
    </div>
</section> <!-- ends classes-sec -->