<section class="coaches-sec">
    <div class="container">
        <div class="title-block"><h2>meet the coaches</h2></div>
        <div class="row">

            <?php
            $cp_type = 'coaches';
            $cp_args = array(
                'post_type'      => $cp_type,
                'post_status'    => 'publish',
                'posts_per_page' => -1,
                'post__in'       => get_sub_field('list_coaches'),
                'orderby'        => 'post__in'
            );

            $cp_my_query = new WP_Query($cp_args);
            //echo '<pre>'. print_r($cp_my_query).'</pre>';
            if ($cp_my_query->have_posts()):

            while ($cp_my_query->have_posts()) : $cp_my_query->the_post();

            ?>

                <div class="col-md-2 col-sm-4 col-xs-6">
                    <div class="item">
                        <a class="ajax-popup-link click-region" href="<?=the_permalink()?>"></a>
                        <div class="img-block">
                            <?= the_post_thumbnail(); ?>
                        </div>
                        <div class="content">
                           <h2><?=the_title()?></h2>
                        </div>
                    </div>
                </div>

            <?php
            endwhile;
            else:
                print("content not found..");
            endif;
            wp_reset_query();

            ?>

        </div>
    </div>
</section>