<?php

/**
 * ReduxFramework Barebones Sample Config File
 * For full documentation, please visit: http://docs.reduxframework.com/
 *
 * For a more extensive sample-config file, you may look at:
 * https://github.com/reduxframework/redux-framework/blob/master/sample/sample-config.php
 *
 */
if (!class_exists('Redux')) {
    return;
}

// This is your option name where all the Redux data is stored.
$opt_name = "arl_options";

/**
 * SET ARGUMENTS
 *
 * All the possible arguments for Redux.
 * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
 */
$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
    'opt_name' => $opt_name,
    'display_name' => $theme->get('Name'),
    'display_version' => $theme->get('Version'),
    'global_variable' => $opt_name,
    'dev_mode' => false,
    'page_title' => 'Theme Options',
    'update_notice' => false,
    'intro_text' => '<p>Customize your theme</p>',
    'footer_text' => '<p>Copyright &copy; All rights reserved</p>',
    'admin_bar' => false,
    'menu_type' => 'menu',
    'menu_title' => 'Theme Options',
    'allow_sub_menu' => false,
    'customizer' => TRUE,
    'default_mark' => '*',
    'hints' => array(
        'icon_position' => 'right',
        'icon_color' => 'lightgray',
        'icon_size' => 'normal',
        'tip_style' => array(
            'color' => 'light',
        ),
        'tip_position' => array(
            'my' => 'top left',
            'at' => 'bottom right',
        ),
        'tip_effect' => array(
            'show' => array(
                'duration' => '500',
                'event' => 'mouseover',
            ),
            'hide' => array(
                'duration' => '500',
                'event' => 'mouseleave unfocus',
            ),
        ),
    ),
    'output' => TRUE,
    'output_tag' => TRUE,
    'compiler' => TRUE,
    'page_permissions' => 'manage_options',
    'save_defaults' => TRUE,
    'show_import_export' => false,
    'database' => 'options',
    'transient_time' => '3600',
    'network_sites' => TRUE
);

Redux::setArgs($opt_name, $args);
/*
 * END ARGUMENTS
 */

/*
 * START HELP TABS
 */
$tabs = array(
    array(
        'id' => 'redux-help-tab-1',
        'title' => __('Theme Information 1', 'admin_folder'),
        'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'admin_folder')
    ),
    array(
        'id' => 'redux-help-tab-2',
        'title' => __('Theme Information 2', 'admin_folder'),
        'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'admin_folder')
    )
);
Redux::setHelpTab($opt_name, $tabs);

// Set the help sidebar
$content = __('<p>This is the sidebar content, HTML is allowed.</p>', 'admin_folder');
Redux::setHelpSidebar($opt_name, $content);
/*
 * END HELP TABS
 */


/*
 *
 * START SECTIONS
 *
 */
// Create general options section
Redux::setSection($opt_name, array(
    'title' => 'General Options',
    'id' => 'general-options',
    'desc' => '',
    'icon' => 'el el-home'
));

// Create header section - submenu of general options
Redux::setSection($opt_name, array(
    'title' => 'Header',
    'id' => 'general-options-header',
    'subsection' => true,
    'desc' => 'Customize theme header area',
    'fields' => array(
        array(
            'id' => 'header-logo',
            'type' => 'media',
            'url' => true,
            'title' => 'Logo',
            'subtitle' => 'Change the logo in theme header',
            'desc' => 'Only use JPEG or PNG file',
            'default' => array(
                'url' => get_template_directory_uri() . '/images/stamina-wellness-logo.jpg'
            ),
        )
    )
));

// Create social section - submenu of general options
Redux::setSection($opt_name, array(
    'title' => 'Social',
    'id' => 'general-options-social',
    'subsection' => true,
    'desc' => 'Customize social links',
    'fields' => array(
        array(
            'id' => 'social-facebook',
            'type' => 'text',
            'title' => 'Facebook',
            'subtitle' => 'Link to facebook page',
            'desc' => '',
            'default' => '',
            'validate' => 'url'
        ),
        array(
            'id' => 'social-twitter',
            'type' => 'text',
            'title' => 'Twitter',
            'subtitle' => 'Link to twitter page',
            'desc' => '',
            'default' => '',
            'validate' => 'url'
        ),
        array(
            'id' => 'social-instagram',
            'type' => 'text',
            'title' => 'Instagram',
            'subtitle' => 'Link to instagram page',
            'desc' => '',
            'default' => '',
            'validate' => 'url'
        ),
        array(
            'id' => 'social-youtube',
            'type' => 'text',
            'title' => 'Youtube',
            'subtitle' => 'Link to youtube page',
            'desc' => '',
            'default' => '',
            'validate' => 'url'
        ),
    )
));

// Create footer section - submenu of general options
Redux::setSection($opt_name, array(
    'title' => 'Footer',
    'id' => 'general-options-footer',
    'subsection' => true,
    'desc' => 'Customize theme footer area',
    'fields' => array(
        array(
            'id' => 'footer-copyright',
            'type' => 'text',
            'title' => 'Copyright Text',
            'subtitle' => 'Change the copyright text in theme footer',
            'desc' => 'Copyright',
            'default' => 'STAMINA 2016',
            'validate' => 'no_html',
        ),
        array(
            'id' => 'footer-logo-left',
            'type' => 'media',
            'url' => true,
            'title' => 'Logo Left',
            'subtitle' => 'Change left log',
            'desc' => 'Only use JPEG or PNG file',
            'default' => array(
                'url' => get_template_directory_uri() . '/images/stamina-wellness-logo.jpg'
            )
        ),
        array(
            'id' => 'footer-logo-right',
            'type' => 'media',
            'url' => true,
            'title' => 'Logo Right',
            'subtitle' => 'Change right log',
            'desc' => 'Only use JPEG or PNG file',
            'default' => array(
                'url' => get_template_directory_uri() . '/images/stamina-wellness-logo.jpg'
            )
        )
    )
));
// Create page options section
Redux::setSection($opt_name, array(
    'title' => 'Contact page',
    'id' => 'contact-options',
    'desc' => '',
    'icon' => 'el el-home',
    'fields' => array(
        array(
            'id' => 'location',
            'type' => 'text',
            'title' => 'Location',
            'subtitle' => 'Add your locations',
            'validate' => 'no_html'
        ),
        array(
            'id' => 'email',
            'type' => 'text',
            'title' => 'Email',
            'subtitle' => 'Add Email Address',
            'validate' => 'no_html'
        ),
        array(
            'id' => 'phone',
            'type' => 'text',
            'title' => 'Phone',
            'subtitle' => 'Add phone number',
            'validate' => ''
        )
    )
));

/*
 *
 * END SECTIONS
 *
 */

/*
 *
 * START HELPER FUNCTIONS
 *
 */

function sqhr_theme_options() {
    global $arl_options;

    return $arl_options;
}

function sqhr_theme_option($key) {
    $options = sqhr_theme_options();

    if (!isset($options[$key]))
        return false;

    if (is_array($options[$key]) || is_object($options[$key]) || strlen($options[$key]))
        return $options[$key];

    return false;
}
