<div class="popup white-popup-block">

    <div class="title-block">
        <h2><?= the_title() ?></h2>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <?= get_the_post_thumbnail(); ?>
        </div>

        <div class="col-sm-8">
            <?php
            while (have_posts()) : the_post();
                the_content();
            endwhile; // End of the loop.
            ?>
        </div>

    </div>
</div>