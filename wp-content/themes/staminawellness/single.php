<?php
get_header('inner');
?>
    <section class="classes-inner">
        <div class="container">

            <div class="title-block">
                <h2><?= the_title() ?></h2>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= get_the_post_thumbnail(); ?>
                </div>

                <div class="col-sm-8">
                    <?php
                    while (have_posts()) : the_post();
                        the_content();
                    endwhile; // End of the loop.
                    ?>
                </div>

            </div>
        </div>
        <!-- #main -->


    </section><!-- #primary -->

<?php
get_footer();
