$(document).ready(function(){
	// Owl carousel
	$(".banner").owlCarousel({
		autoplay: true,
		autoplayHoverPause: true,
		autoplayTimeout: 4000,
		autoplaySpeed: false,
        smartSpeed: 1500,
        items:1,
        loop:true,
        margin:0,
        nav:true,
        dots:false
    });
    $(".classes-sec .items").owlCarousel({
        autoplay: true,
        autoplayHoverPause: true,
        autoplayTimeout: 3000,
        autoplaySpeed: false,
        smartSpeed: 1000,
        items:5,
        loop:true,
        margin:0,
        nav:true,
        dots:false,
        responsiveClass:true,
        responsive:{
            0:{
                items:1
            },
            481:{
                items:2,
            },
            768:{
                items:3,
            },
            991:{
                items:4,
            },
            1200:{
                items:5,
            }
        }
    });
    $("#owl-classes").owlCarousel({
        autoplay: true,
        autoplayHoverPause: true,
        autoplayTimeout: 3000,
        autoplaySpeed: false,
        smartSpeed: 1000,
        items:3,
        loop:true,
        margin:30,
        nav:true,
        dots:false,
        responsiveClass:true,
        responsive:{
            0:{
                items:1
            },
            481:{
                items:2,
            },
            768:{
                items:2,
            },
            991:{
                items:3,
            }
        }
    });

    if ($('.banner-sec').length) {        
        function sectionHeight(){
            jQuery('.banner-sec .content-wrap').css({
                height: $('.banner-sec').innerHeight()
            });
        }
        sectionHeight();
        jQuery(window).resize(function() {
           sectionHeight();         
        });
    }
    $(".the-gym-sec .item").click(function() {
        $(this).toggleClass("active");
        $('.the-gym-sec .item').not($(this)).removeClass('active');
    });
    $('.ajax-popup-link').magnificPopup({
        type: 'ajax'
    });
});