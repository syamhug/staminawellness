<?php

/**
 * Main admin side class for plugin
 */
class DPI_Contact_Form_Admin {

    protected $version;

    public function __construct($version) {
        $this->version = $version;
    }

    function render_contact_form($atts) {
        return get_contact_form_html();
    }

    function add_list_contact_form_page_nav() {
        add_menu_page('Booking', 'Booking', 'publish_posts', 'queries_contact_form', array($this, 'list_contact_form_page'), 'dashicons-format-chat', '81.1');
    }

    /**
     * Options page - Main page, callback function of add menu
     */
    function list_contact_form_page() {
        // Db
        global $wpdb;

        // Include required file
        include(plugin_dir_path(dirname(__FILE__)) . 'includes/contact-form-list-data.php');

        // Set table name
        $table_name = $wpdb->prefix . "contact_form";

        // Check if table already exists else create one
        if ($wpdb->get_var("SHOW TABLES LIKE '{$table_name}'") != $table_name) {
            // Create Table
            include(plugin_dir_path(dirname(__FILE__)) . 'includes/class.db.queries.php');
            new DbQueries();
        }

        // Get the number of results
        $total_listings = $wpdb->query("SELECT * FROM $table_name");
        ?>  
        <div class="wrap">  
            <h2>Booking Details</h2>

            <?php
            if (!$total_listings) {
                echo '<div class="error">No queries until now..</div>';
            } else {
                $wp_list_table = new ContactForm_List_Table();
                $wp_list_table->prepare_items();
                $wp_list_table->display();
            }
            ?>
        </div>  
        <?php
    }

}
