$(document).ready(function () {

    /**
     * JSON Parser
     */
    if (!parse_JSON) {
        var parse_JSON = function (data) {
            try {
                var response = $.parseJSON(data);
                var obj = JSON && JSON.parse(data) || $.parseJSON(data);
                return obj;
            } catch (e) {
                // not json
                console.log(data);
                alert("Oops, it looks like there is an issue, we are looking to fix it");
                return false;
            }

        };
    }

    /**
     * Form submit
     */
    $(document).on('submit', '.form-contactus', function (e) {
        e.preventDefault();

        var form = this;
        var datastring = $(form).serialize();
        datastring += '&action=contact_form_callback';
        var submit_btn = $(form).find('button[type=submit]');

        submit_btn.attr('disabled', 'disabled');

        $.post(ajaxurl, datastring, function (data) {
            var response = parse_JSON(data);
            if (response.status === "success") {
                $(form)[0].reset();
            }
            alert_contact_form(response.message);
            submit_btn.removeAttr("disabled");
        });

        return false;
    });

    var alert_contact_form = function ($str) {
        $('#contact-modal .content').html($str);
        $.magnificPopup.open({
            items: {
                src: $('#contact-modal')[0].outerHTML
            },
            type: 'inline',
            modal: true
        });
    };

    $(document).on('click', '.contact-modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });

});