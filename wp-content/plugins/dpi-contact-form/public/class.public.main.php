<?php

/**
 * Plugin public modules loader and handler
 */
class DPI_Contact_Form_Public {

    protected $version;

    public function __construct($version) {
        $this->version = $version;
    }

    /**
     * Enqueue public side scripts
     * 
     * @param string $hook Post page 
     */
    public function enqueue_scripts($hook) {
        wp_enqueue_script('jquery');
        wp_enqueue_style('magnific-css', plugin_dir_url(__FILE__) . 'css/magnific-popup.css', array(), '1.0.0', 'all');
        wp_enqueue_script('magnific', plugin_dir_url(__FILE__) . 'js/jquery.magnific-popup.min.js', array(), '3.3.6', true);
        wp_enqueue_script(
                'dpi-contact-form-public-script', plugin_dir_url(__FILE__) . 'js/contact.min.js', array(), $this->version, TRUE
        );
    }

    /**
     * AJAX callback function, used to store rating
     * 
     * @global array $ratings_range Rating range
     */
    public function contact_form_callback() {
        // Gather data
        $first_name = sanitize_text_field($_POST['cf_firstname']);
        $last_name = sanitize_text_field($_POST['cf_lastname']);
        $email = sanitize_text_field($_POST['cf_email']);
        $mobile = sanitize_text_field($_POST['cf_mobile']);
        $age = sanitize_text_field($_POST['cf_age']);
        $classname = sanitize_text_field($_POST['cf_classname']);

        // Validate
        if (strlen($first_name) < 1) {
            _djsn("Please enter your First Name");
        }
        if (strlen($last_name) < 1) {
            _djsn("Please enter your Last Name");
        }
        if (strlen($email) < 1) {
            _djsn("Please enter your Email Address");
        }
        if (!is_email($email)) {
            _djsn("It looks like the Email Address you provided is not valid");
        }
        if (strlen($mobile) < 1) {
            _djsn("Please enter your Mobile Number");
        }
        if (!is_numeric($mobile) || strlen($mobile) < 7) {
            _djsn("Invalid Mobile Number");
        }
        if (strlen($age) < 1 ) {
            _djsn("Please enter your Age");
        } else if ($age > 72 || $age < 10 || !is_numeric($age)) {
            _djsn("Your Age is not valid.");
        }
        if (strlen($classname) < 1) {
            _djsn("Please enter your Class Name");
        }

        // Include dependent class
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/class.queries.php';

        // Initialize object
        $subscription = new Queries();

        // Save the data
        $save = $subscription->save($first_name, $last_name, $email, $mobile, $age, $classname);

        // Return appropriate response
        if ($save) {
            _djsn("Thank you for your booking, we will get back to you shortly.", "success");
        } else {
            _djsn("It looks like there is an issue, we are looking to fix it.");
        }
    }

}
