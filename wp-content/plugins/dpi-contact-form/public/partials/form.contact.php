<style type="text/css">
    .contact-popup {
        background: white;
        width: 50%;
        padding: 10px;
        border-radius: 5px;
    }
    .contact-modal-dismiss {
        float: right;
        font-family: 'Neutra2Display-Medium'!important;
        width: 100px;
        height: 50px;
        font-size: 19px;
        padding: 0px 0px;
    }
</style>
<div class="hide">
    <div id="contact-modal" class="contact-popup container">
        <h4 class="text-center content"></h4>
        <br />
        <button class="contact-modal-dismiss btn" href="#"><?php _e('OK', 'sqhr'); ?></button>
    </div>
</div>

<form class="form-horizontal feedback form-contactus" method="POST">
    <div class="row">
        <div class="col-sm-6">
            <label>First Name</label>
            <div class="form-group">
                <input name="cf_firstname" type="text" class="form-control" id="fname">
            </div>
        </div>
        <div class="col-sm-6">
            <label>Last Name</label>
            <div class="form-group">
                <input name="cf_lastname" type="text" class="form-control" id="lname">
            </div>
        </div>
        <div class="col-sm-12">
            <label>Email Address</label>
            <div class="form-group">
                <input name="cf_email" type="email" class="form-control" id="email">
            </div>
        </div>
        <div class="col-sm-12">
            <label>Mobile</label>
            <div class="form-group">
                <input name="cf_mobile" type="text" class="form-control" id="mobile">
            </div>
        </div>
        <div class="col-sm-6">
            <label>Age</label>
            <div class="form-group">
                <input name="cf_age" type="text" class="form-control" id="age">
            </div>
        </div>
        <div class="col-sm-6">
            <label>Class Name</label>
            <div class="form-group">
                <input name="cf_classname" type="text" class="form-control" id="classname">
            </div>
        </div>
        <div class="col-sm-12">
            <div class="actions">
                <button type="submit" class="btn"><?php _e('SIGN UP', 'sqhr') ?></button>
            </div>
        </div>
    </div>
</form>