<?php

/**
 * Queries - DPI 
 *
 * Plugin bootstrap file. This plugin adds rating capbility for default 
 * and custom post types
 *
 * @link              http://hugdigital.com
 * @since             1.0.0
 * @package           Queries
 *
 * @wordpress-plugin
 * Plugin Name:       Queries
 * Plugin URI:        http://hugdigital.com
 * Description:       Contact form for DPI website
 * Version:           1.0.0
 * Author:            Syed Qarib
 * Author URI:        http://hugdigital.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       dpi-subscribe-form
 * Domain Path:       /languages
 */
/**
 * If this file is called directly, abort.
 */
if (!defined('WPINC')) {
    die;
}

/**
 * Include base files
 */
require_once plugin_dir_path(__FILE__) . 'includes/constants.php';
require_once plugin_dir_path(__FILE__) . 'includes/helpers.php';
require_once plugin_dir_path(__FILE__) . 'includes/class.main.php';

/**
 * Initiate the plugin
 */
function init_dpi_contact_form() {
    $spr = new DPI_Contact_Form();
    $spr->run();
}

init_dpi_contact_form();
