<?php

/**
 * Ratings main class
 * 
 * Class responsible for adding and returing ratings information
 */
class Queries {

    private $db_queries;

    public function __construct() {
        $this->load_dependencies();

        $this->db_queries = new DbQueries();
    }

    /**
     * Load dependent modules
     */
    private function load_dependencies() {
        require_once plugin_dir_path(__FILE__) . 'class.db.queries.php';
    }

    function save($first_name, $last_name, $email, $mobile, $age, $classname) {
        $this->db_queries->user_id = get_current_user_id();
        $this->db_queries->first_name = $first_name;
        $this->db_queries->last_name = $last_name;
        $this->db_queries->email = $email;
        $this->db_queries->mobile = $mobile;
        $this->db_queries->age = $age;
        $this->db_queries->class_name = $classname;
        $this->db_queries->IP = get_client_ip();
        $this->db_queries->browser = serialize(getBrowser());
        $this->db_queries->DOC = time();
        $this->db_queries->status = 0;

        return $this->db_queries->add();
    }

}
