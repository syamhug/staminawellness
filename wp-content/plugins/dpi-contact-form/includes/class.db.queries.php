<?php

/**
 * Queries database class
 * 
 * Uses $wpdb Wordpress database instance
 */
class DbQueries {

    private $db;
    private $table_name;
    private $charset;
    public $id;
    public $user_id;
    public $first_name;
    public $last_name;
    public $email;
    public $mobile;
    public $age;
    public $class_name;
    public $IP;
    public $browser;
    public $DOC;
    public $status;

    public function __construct() {
        global $wpdb;

        $this->db = $wpdb;
        $this->table_name = $this->db->prefix . 'contact_form';
        $this->charset = $this->db->get_charset_collate();
        $this->_create_table();
    }

    /**
     * Create table
     */
    public function _create_table() {
        $sql = "CREATE TABLE IF NOT EXISTS $this->table_name (
                id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                user_id int(11) UNSIGNED NOT NULL,
                first_name varchar(50) NOT NULL,
                last_name varchar(50) NOT NULL,
                email varchar(150) NOT NULL,
                mobile varchar(100) NOT NULL,
                age varchar(100) NOT NULL,
                class_name text NOT NULL,
                IP varchar(50) NOT NULL,
                browser text NOT NULL,
                DOC int(11) UNSIGNED NOT NULL,
                status tinyint NOT NULL,
                UNIQUE KEY id (id),
                PRIMARY KEY (id)
          ) $this->charset;";

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta($sql);
    }

    /**
     * Add or update record
     *  
     * @return boolean/int Status/Post id
     */
    public function add() {
        $insert = $this->db->insert(
                $this->table_name, array(
            'user_id' => $this->user_id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'mobile' => $this->mobile,
            'age' => $this->age,
            'class_name' => $this->class_name,
            'IP' => $this->IP,
            'browser' => $this->browser,
            'DOC' => $this->DOC,
            'status' => $this->status
                ), array('%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%d')
        );

        return ($insert) ? $this->db->insert_id : $insert;
    }

}
