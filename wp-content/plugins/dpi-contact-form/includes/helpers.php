<?php

/**
 * Helper functions for plugin
 * 
 * These functions will be available globally when this plugin is activated
 */
/**
 * If this file is called directly, abort.
 */
if (!defined('WPINC')) {
    die;
}

function contact_form_countries_list() {
    // Access country list, defined in country selector functions.php->module
    $countries_list = array('India','Germany');

    $options = '';

    foreach ($countries_list as $country) {
        $options .= "<option value=\"{$country}\">{$country}</option>";
    }

    return $options;
}

if (!function_exists('get_contact_form_html')) {

    /**
     * Prints form to add rating, use this within WP Loop
     */
    function get_contact_form_html() {
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/partials/form.contact.php';
    }

}

if (!function_exists('_djsn')) {

    /**
     * Terminate with printing message and status in JSON format
     * 
     * @param string $str Message
     * @param string $status Status (error, success ..)
     */
    function _djsn($str, $status = "error") {
        echo json_encode(array("status" => $status, "message" => $str));
        exit();
    }

}
