<?php

/**
 * Import required table listing class
 */
if (!class_exists('WP_List_Table')) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

/**
 * Custom table listing class
 */
class ContactForm_List_Table extends WP_List_Table {

    /**
     * Override the parent constructor to pass custom parameters
     */
    function __construct() {
        parent::__construct(array(
            'singular' => 'wp_list_contact_form', // Singular label
            'plural' => 'wp_list_contact_form', // Plural label
            'ajax' => false // AJAX support ?
        ));
    }

    /**
     * Extra navigation
     * 
     * @param string $which
     */
    function extra_tablenav($which) {
        if ($which == "top") {
            // Code that goes before the table
        }
        if ($which == "bottom") {
            // Code that goes after the table
        }
    }

    /**
     * Define columns and their titles
     * 
     * @return array
     */
    function get_columns() {
        return $columns = array(
            'col_id' => __('SNo'),
            'col_first_name' => __('First Name'),
            'col_last_name' => __('Last Name'),
            'col_email' => __('Email'),
            'col_mobile' => __('Mobile'),
            'col_age' => __('Age'),
            'col_class_name' => __('Class Name'),
            'col_DOC' => __('Date')
        );
    }

    /**
     * Define sortable columns
     * 
     * @return array
     */
    public function get_sortable_columns() {
        return $sortable = array(
            'col_id' => array('id', true),
            'col_first_name' => array('first_name', true),
            'col_last_name' => array('last_name', true),
            'col_email' => array('email', true),
            'col_mobile' => array('mobile', true),
            'col_age' => array('age', true),
            'col_DOC' => array('DOC', true)
        );
    }

    /**
     * Fetch and Prepare table data
     * 
     * @global object $wpdb
     * @global array $_wp_column_headers
     */
    function prepare_items() {
        global $wpdb;

        // Table name
        $table_name = $wpdb->prefix . "contact_form";

        // Prepare query
        $query = "SELECT * FROM $table_name";

        // Order parameters
        $orderby = !empty($_GET["orderby"]) ? mysql_real_escape_string($_GET["orderby"]) : 'ASC';
        $order = !empty($_GET["order"]) ? mysql_real_escape_string($_GET["order"]) : '';
        if (!empty($orderby) & !empty($order)) {
            $query.=' ORDER BY ' . $orderby . ' ' . $order;
        }

        // Pagination parameters
        // Number of elements in table
        // Returns the total number of affected rows
        $totalitems = $wpdb->query($query);

        // How many records to display per page
        $perpage = 10;

        // Get the current page
        $paged = !empty($_GET["paged"]) ? mysql_real_escape_string($_GET["paged"]) : '';
        if (empty($paged) || !is_numeric($paged) || $paged <= 0) {
            $paged = 1;
        }

        // Count Total pages
        $totalpages = ceil($totalitems / $perpage);

        // Add pagination parameters to query
        if (!empty($paged) && !empty($perpage)) {
            $offset = ($paged - 1) * $perpage;
            $query.=' LIMIT ' . (int) $offset . ',' . (int) $perpage;
        }

        // Register pagination
        // The pagination links are automatically built according to these parameters
        $this->set_pagination_args(array(
            "total_items" => $totalitems,
            "total_pages" => $totalpages,
            "per_page" => $perpage,
        ));

        // Register the columns
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);

        // Fetch results and store in local variable
        $this->items = $wpdb->get_results($query);
    }

    function display_rows() {
        // Get the records fetched by prepare_items
        $records = $this->items;

        // Get the columns registered in the get_columns and get_sortable_columns methods
        list( $columns, $hidden ) = $this->get_column_info();

        // Loop for each record
        foreach ($records as $rec) {
            // Row
            echo '<tr id="record_' . $rec->id . '">';
            foreach ($columns as $column_name => $column_display_name) {

                // Style attributes for each column
                $class = "class='$column_name column-$column_name'";
                $style = (in_array($column_name, $hidden)) ? ' style="display:none;"' : "";
                $attributes = $class . $style;

                // Display the cell
                switch ($column_name) {
                    case "col_id": echo '<td ' . $attributes . '>' . stripslashes($rec->id) . '</td>';
                        break;
                    case "col_first_name": echo '<td ' . $attributes . '>' . stripslashes($rec->first_name) . '</td>';
                        break;
                    case "col_last_name": echo '<td ' . $attributes . '>' . stripslashes($rec->last_name) . '</td>';
                        break;
                    case "col_email": echo '<td ' . $attributes . '>' . stripslashes($rec->email) . '</td>';
                        break;
                    case "col_mobile": echo '<td ' . $attributes . '>' . stripslashes($rec->mobile) . '</td>';
                        break;
                    case "col_age": echo '<td ' . $attributes . '>' . stripslashes($rec->age) . '</td>';
                        break;
                    case "col_class_name": echo '<td ' . $attributes . '>' . stripslashes($rec->class_name) . '</td>';
                        break;
                    case "col_DOC": echo '<td ' . $attributes . '>' . $this->parseDate($rec->DOC) . '</td>';
                        break;
                }
            }

            echo'</tr>';
        }
    }

    /**
     * Parse date
     * 
     * @param UNIXTIMESTAMP $timestamp
     * @return string Human readable date
     */
    function parseDate($timestamp) {
        return date("d M Y", $timestamp);
    }

}
