<?php

/**
 * Plugin startup class
 * 
 * Used for firing up all modules (Admin, Public)
 */
class DPI_Contact_Form {

    protected $loader;
    protected $plugin_slug;
    protected $version;

    public function __construct() {

        $this->plugin_slug = 'dpi-contact-form';
        $this->version = '1.0.0';

        $this->load_dependencies();
        $this->define_public_hooks();
        $this->define_admin_hooks();
    }

    /**
     * Attach plugin dependencies and initiate loader
     */
    private function load_dependencies() {
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/class.public.main.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/class.admin.main.php';
        require_once plugin_dir_path(__FILE__) . 'class.loader.php';

        $this->loader = new DPI_Contact_Form_Loader();
    }

    /**
     * Define public side hooks
     */
    private function define_public_hooks() {
        $public = new DPI_Contact_Form_Public($this->get_version());

        $this->loader->add_action('wp_enqueue_scripts', $public, 'enqueue_scripts');
        $this->loader->add_action('wp_ajax_contact_form_callback', $public, 'contact_form_callback'); // For logged in users
        $this->loader->add_action('wp_ajax_nopriv_contact_form_callback', $public, 'contact_form_callback'); // For non logged in users


    }

    /**
     * Define admin side hooks
     */
    private function define_admin_hooks() {
        $admin = new DPI_Contact_Form_Admin($this->get_version());

        $this->loader->add_action('admin_menu', $admin, 'add_list_contact_form_page_nav');
    }

    /**
     * Run loader
     */
    public function run() {
        $this->loader->run();

        // Add shortcode support
        $admin = new DPI_Contact_form_Admin($this->get_version());
        add_shortcode('contactform', array($admin, 'render_contact_form'));
    }

    /**
     * Returns plugin version number
     * 
     * @return string Plugin version number
     */
    public function get_version() {
        return $this->version;
    }

}
